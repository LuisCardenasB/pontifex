<?php
include_once 'conn.php';
$suma = 0;
$rows = $_REQUEST['rows'];
$columna = $_REQUEST['columna'];
$id = $_REQUEST['id'];
$row = json_encode($rows);
$no_renglon = 0;
/**
 * Se realiza la consulta a la tabla vtiger_potential donde obtendremos el numero de Integración
 */
$potential_no = mysqli_query($con, "SELECT potential_no FROM vtiger_potential WHERE potentialid = $id")or die (mysqli_error());

/**
 * Obtenemos el numero de renglon para poder concatenarlo en la clave
 */
foreach ($rows as $llave => $valor) {
    if (isset($valor['productid'])) {
        if ($valor['productid'] == 'Credito - Simple' || $valor['productid'] == 'Credito - Revolvente' || $valor['productid'] == 'Arrendamiento - Leasing' || $valor['productid'] == 'Arrendamiento - Lease back' || $valor['productid'] == 'Factoraje - Factoraje') {
            $no_renglon = count($rows);
        }   
    }
}
/**
 * Concatenamos el numero de integracion y el numero de renglon para crear la clave
 */
foreach ($potential_no as $key_po => $potential) {
    $potential_no = $potential['potential_no'].'_';
}
/**
 * Agregamos la clave al campo op donde se mostrara la clave
 */
$valor = [];
$count_vueltas = 1;
foreach ($rows as $key_row => $row) {
    for ($i=$count_vueltas; $i <= $no_renglon; $i++) { 
        $clave = $potential_no.$i;
        if ($row['op'] == '') {
            $row['op'] = $clave;
        }
        break 1;
    }
$count_vueltas++;
    array_push($valor, $row);
}

$row = json_encode($valor);
/**
 * Se realiza la modificacion y guarda la información del json
 */
mysqli_query($con,"UPDATE vtiger_potentialscf SET $columna = '$row' WHERE potentialid = $id")or die (mysqli_error());

// Se realiza la suma del campo monto el cual se va a guardar en la columna amount que se encuentra en la tabla
// vtiger_potential
foreach ($rows as $key => $value) {
    if (isset($value['monto'])) {
        $total = $value['monto'];
        $suma = $suma + $total;
    }
}

if ($suma) {
mysqli_query($con,"UPDATE vtiger_potential SET amount = $suma WHERE potentialid = $id")or die(mysqli_error());
}

