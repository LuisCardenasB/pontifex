<?php
include_once '../conn.php';
$return = Array('ok' => 1, 'msg' => "Su archivo se ha subido correctamente");
$estatus = 1;
$id = $_POST['id_documento'];
$nombre = $_POST['nombre_documento'];
$num_operacion = $_POST['num_operacion'];
$nombre_institucion = $_POST['nombre_institucion'];
$nombre_oportunidad = $_POST['nombre_oportunidad'];

if (empty($_FILES))
{
    $return = Array('ok' => 3,'msg' => "Se debe de seleccionar el documento a subir.", 'status' => 'error');
}
else {
    foreach ($_FILES as $key) {
        // Condicional si el fichero existe
        if ($key['error'] == UPLOAD_ERR_OK) {
            // Nombres de archivos de temporales
            $archivonombre = $key['name'];
            $fuente = $key['tmp_name'];
            $exten = explode(".", $key['name']);
            $extencion = end($exten);

            $carpeta = $num_operacion.'/';

            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777) or die("Hubo un error al crear el directorio de almacenamiento");
            }
            $nombre = str_replace('/', '-', $nombre);

            $target_path = $carpeta.''.$nombre.'.'.$extencion; // Indicamos la ruta de destino de los archivos

            if (file_exists($target_path)) {
                $return = Array('ok' => 2,'msg' => "El documento ".$nombre." ya existe.", 'status' => 'error');
                $estatus = 0;
            }

            if (!move_uploaded_file($fuente, $target_path)) {
                $return = Array('ok' => 0, 'msg' => "Ocurrio un error al subir el archivo. ".$nombre." No pudo guardarse.", 'status' => 'error');
                $estatus = 0;
            }
        }
    }
    if ($estatus == 1) {
        mysqli_query($con, "INSERT INTO vtiger_document_upload (`num_operacion`,`nom_file`,`ruta`,`comentario`
                              ,`nombre_oportunidad`,`Institucion`, `documentid`,`status`) VALUES ('$num_operacion','$nombre','$target_path', 
                              null, '$nombre_oportunidad', '$nombre_institucion','$id','$estatus')");
    }
}
header('Content-Type: application/json');
echo json_encode($return);