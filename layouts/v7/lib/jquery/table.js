var editIndex = undefined;
var url;
var id;
var num;
var name;
var institution_name;
var documento;
function endEditing(tbl){
    if (editIndex == undefined){return true}
    if ($(tbl).datagrid('validateRow', editIndex)){
        $(tbl).datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onClickCell(index, field){
    var tbl = "#"+this.id;
    if (editIndex != index){
        if (endEditing(tbl)){

            $(tbl).datagrid('selectRow', index)
                .datagrid('beginEdit', index);
            var ed = $(tbl).datagrid('getEditor', {index:index,field:field});

            if (tbl){
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
            }
            editIndex = index;
        } else {
            setTimeout(function(){
                $(tbl).datagrid('selectRow', editIndex);
            },0);
        }
    }
}
function onEndEdit(index, row){
    var ed = $(this).datagrid('getEditor', {
        index: index,
        field: 'productid'
    });
     //row.value = $(ed.target).combobox('getText');
}
function append(tbl){
    if (endEditing(tbl)){
        $(tbl).datagrid('appendRow',{status:'P'});
        editIndex = $(tbl).datagrid('getRows').length-1;
        $(tbl).datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function accept(columna, id_formulario,tbl){
    if (endEditing(tbl)){
        $(tbl).datagrid('acceptChanges');
        var rows = $(tbl).datagrid('getRows');
        $.ajax("modules/Settings/LayoutEditor/models/solicitud/Solicitud.php", {
            type:'POST',
            dataType: 'json',
            data:{rows, "columna":columna, "id":id_formulario}
        });
        $(tbl).datagrid('reload');    // reload the current page data
        save_log(rows,id_formulario,columna);
    }
}
function reject(tbl){
    $(tbl).datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges(tbl){
    var rows = $(tbl).datagrid('getChanges');
    alert(rows.length+' rows are changed!');
}
function initialize(tbl){
    var url = $(tbl).data('url');
    var fields = $(tbl).data('fields');
    var columns = [];

    var s =fields;
    var myObject = eval('(' + s + ')');
    
    for (i in myObject)
    {
        columns.push(myObject[i]);
    }

    var options = {
        iconCls: 'icon-edit',
        singleSelect: true,
        toolbar: '#tb',
        url: url,
        method: 'get',
        onClickCell: onClickCell,
        onEndEdit: onEndEdit,
        fitColumns: true,
        rownumbers:true,
    };

    options.columns = columns;
    $(tbl).datagrid(options);
}
function subir_archivo(){
    var archivos = document.getElementById("rutas_file_document");
    var archivo = archivos.files;
    var archivos = new FormData();
    // var parametros = new FormData($("#formulario-envia"));
    // modules/Settings/LayoutEditor/models/solicitud/
    for (i = 0; i < archivo.length; i++) {
        archivos.append('rutas_file'+i, archivo[i]);
    }
    archivos.append('id_documento', id);
    archivos.append('nombre_documento', documento);
    archivos.append('num_operacion', num);
    archivos.append('nombre_institucion', institution_name);
    archivos.append('nombre_oportunidad', name);
    $.ajax({
        url:"zipfiles/insert_document.php",
        type: "POST",
        data: archivos,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.ok == 1) {
                alert(response.msg);
                $("#xxxzzz").modal("hide");
                $("#rutas_file_document").val('');
            }

            if (response.ok == 2) {
                alert(response.msg);
                $("#rutas_file_document").val('');
            }
        	if (response.ok == 3) {
                alert(response.msg);
            }
        }
    });
}
function zip_file() {
    $.ajax({
        type:'POST',
        url: "zipfiles/zip_download.php",
        data:{"num_operacion":num},
    	success:function(data){
        	window.open(data, "Archivo", "width=300, height=200")
        }
    });
}
function indice (name_document,id_document) {
	id = id_document;
    documento = name_document;
}
function num_operacion(num_ope) {
    $.each(num_ope, function(k, data){
        num = data.op;
        institution_name = data.institution;
    });
}
function function_name(potential_name) {    
  name = potential_name;
}
function uploadSelected(tbl,potential_name) 
{
    var rows = $(tbl).datagrid('getSelections');
    num_operacion(rows);
	function_name(potential_name);
    $.ajax({
        type:'POST',
        url:"modules/Settings/LayoutEditor/models/solicitud/get_document.php",
        data:{rows,"nombre_oportunidad":potential_name},
        beforeSend:function() {
            $('#tabla tbody').empty();
            $('#tabla thead').empty();
            $('#history tbody').empty();
            $('#history thead').empty();
        },
        success:function(data){
            if (data) {
                html = '';
                thead ='';
                thead += '<tr>'
                    thead += '<th> # </th>'
                    thead += '<th> Documentos </th>'
                    thead += '<th></th>'
                    thead += '</tr>'
           			html += '<tr>'
                    html += '<td><a href="#" class="fa fa-download" onclick="zip_file()"> Generar Zip</a></td>'
                    html += '</tr>'
                $.each(data, function(i,item){
                    html += '<tr>'
                    html += '<td class='+i+'>'+item.documentid+'</td>'
                    html += '<td class='+i+'>'+item.documentname+'</td>'
                    html += '<td><a href="#" class="fa fa-upload" data-toggle="modal" data-target="#xxxzzz" onclick="indice(\''+item.documentname+'\',\''+item.documentid+'\')"> Subir Documento</a></td>'
                	if (item.estatus == 1)
                    {
                        html += '<td class='+i+'><i class="fa fa-check-circle fa-2x" style="color:#00722e;"></i></td>'
                    }
                    else
                    {
                        html += '<td class='+i+'><i class="fa fa-times-circle fa-2x" style="color:#cc0605;"></i></td>'
                    }
                    html += '</tr>'
                    html += '</tr>'
                });
                    
                $('#tabla tbody').append(html);
                $('#tabla thead').append(thead);
            }
        	else {
            	html = '';
                thead ='';
                thead += '<tr>'
                    thead += '<th> # </th>'
                    thead += '<th> Documentos </th>'
                    thead += '</tr>'
                    html += '<tr>'
                    html += '<td colspan="2">Es necesario seleccionar una ruta para poder mostrar la documentación correspondiente</td>'
                    html += '</tr>'
            $('#tabla tbody').append(html);
            $('#tabla thead').append(thead);
        	}
        },
        error:function(error,code){
            
        }
    });
}
function save_log(row,id_formulario,columna) {
    $.ajax("modules/Settings/LayoutEditor/models/solicitud/save_log.php",{
        type:'POST',
        dataType:'json',
        data: {row,"columna":columna, "id":id_formulario}
    });
}
function get_history (tbl) {
    var rows = $(tbl).datagrid('getSelections');
    
    $.ajax({
        type:'POST',
        url:"modules/Settings/LayoutEditor/models/solicitud/get_history.php",
        data:{rows},
        beforeSend:function() {
            $('#history tbody').empty();
            $('#history thead').empty();
            $('#tabla tbody').empty();
            $('#tabla thead').empty();
        },
        success:function(data){
            if (data) {
                html = '';
                thead ='';
                thead += '<tr>'
                    thead += '<th> Numero de Operador </th>'
                    thead += '<th> Estatus </th>'
                    thead += '<th> Fecha de modificación </th>'
                    thead += '</tr>'
                $.each(data, function(i,item){
                    html += '<tr>'
                    html += '<td>'+item.num_operacion+'</td>'
                    html += '<td>'+item.status+'</td>'
                    html += '<td>'+item.date_update+'</td>'
                    html += '</tr>'
                });
                $('#history tbody').append(html);
                $('#history thead').append(thead);
            }
          else{
          		html = '';
                thead ='';
                thead += '<tr>'
                    thead += '<th> Numero de Operador </th>'
                    thead += '<th> Estatus </th>'
                    thead += '<th> Fecha de modificación </th>'
                    thead += '</tr>'
                    html += '<tr>'
                    html += '<td colspan="3">Es necesario seleccionar una ruta para poder mostrar el historial de movimientos.</td>'
                    html += '</tr>'
                $('#history tbody').append(html);
                $('#history thead').append(thead);
          }
        },
        error:function(error,code)
        {

        }
    });
}
