<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * Portions created by JPL TSolucio, S.L. are Copyright (C) jpl tsolucio.
 * All Rights Reserved.
 * ********************************************************************************
 *  Language     : Español es_es
 *  Version      : 6.0.0
 *  Created Date : 2012-10-26
 *  Author       : JPL TSolucio, S. L. Joe Bordes
 *  Last change  : 2013-05-03
 *  Author       : JPL TSolucio, S. L. Joe Bordes
 ************************************************************************************/
$languageStrings = array(
	'Potentials'                   => 'Integraciones',
	'SINGLE_Potentials'            => 'Integración',
	'LBL_ADD_RECORD'               => 'Agregar Integración',
	'LBL_RECORDS_LIST'             => 'Listado de Integraciones',
	'LBL_OPPORTUNITY_INFORMATION'  => 'Generales:',
	'Potential No'                 => 'Núm. OIntegración',
	'Amount'                       => 'Importe',
	'Next Step'                    => 'Siguiente Paso',
	'Sales Stage'                  => 'Fase de venta',
	'Probability'                  => 'Probabilidad',
	'Campaign Source'              => 'Campaña origen',
	'Forecast Amount'              => 'Ingresos ponderado',
	'Funnel'                       => 'Embudo de Ventas',
	'Potentials by Stage'          => 'Integraciónes por fase',
	'Total Revenue'                => 'Ingresos por vendedor',
	'Top Potentials'               => 'Integraciones más importantes',
	'Forecast'                     => 'Presupuesto de ventas',
	'Prospecting'                  => 'Investigando',
	'Qualification'                => 'Calificando',
	'Needs Analysis'               => 'Necesita análisis',
	'Value Proposition'            => 'Propuesta de evaluación',
	'Id. Decision Makers'          => 'Identificando Quien Decide',
	'Perception Analysis'          => 'Análisis',
	'Proposal/Price Quote'         => 'Cotización propuesta',
	'Negotiation/Review'           => 'Negociando/Revisando',
	'Closed Won'                   => 'Cerrada-Ganada',
	'Closed Lost'                  => 'Cerrada-Perdida',
	'--None--'                     => '-----',
	'Existing Business'            => 'Negocio existente',
	'New Business'                 => 'Nuevo negocio',
	'LBL_EXPECTED_CLOSE_DATE_ON'   => 'Fecha esperada de cierre',
	'LBL_RELATED_CONTACTS' => 'Contactos Relacionados',
	'LBL_RELATED_PRODUCTS' => 'Productos Relacionados',
    'Related To'           => 'Nombre de la Organización',
    'Type'                         => 'Tipo',
    
    //Convert Potentials
    'LBL_CONVERT_POTENTIAL' => 'Convertir Integraciones',
    'LBL_POTENTIALS_FIELD_MAPPING' => 'Asignación de campos de Integraciones',
    'LBL_CONVERT_POTENTIALS_ERROR' => 'Debe habilitar su proyecto para convertir la Integración',
    'LBL_POTENTIALS_FIELD_MAPPING_INCOMPLETE' => 'La asignación de campos de Integración está incompleta (Ajustes> Gestor de módulos> Integraciones> Asignación de campos de Integración)',
	'LBL_CREATE_PROJECT'           => 'Crear proyecto'              , 
    
    //Potentials Custom Field Mapping
	'LBL_CUSTOM_FIELD_MAPPING'=> 'Asignación de Integración a Proyecto',

  'Contact Name' => 'Nombre del contacto',

);

$jsLanguageStrings = array(
	'JS_SELECT_PROJECT_TO_CONVERT_LEAD' => 'La conversión requiere seleccionar un Proyecto',
);