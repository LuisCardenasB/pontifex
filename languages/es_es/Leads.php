<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * Portions created by JPL TSolucio, S.L. are Copyright (C) jpl tsolucio.
 * All Rights Reserved.
 * ********************************************************************************
 *  Language     : Español es_es
 *  Version      : 6.0.0
 *  Created Date : 2012-10-25
 *  Author       : JPL TSolucio, S. L. Joe Bordes
 *  Last change  : 2013-05-03
 *  Author       : JPL TSolucio, S. L. Joe Bordes
 ************************************************************************************/
$languageStrings = array(
	'Leads'                        => 'Prospectos',
	'SINGLE_Leads'                 => 'Prospecto',
	'LBL_RECORDS_LIST'             => 'Lista de Prospecto',
	'LBL_ADD_RECORD'               => 'Agregar Prospecto',
	'LBL_LEAD_INFORMATION'         => 'Información del Prospecto',
	'Lead No'                      => 'Núm. de Prospecto',
	'Company'                      => 'Empresas',
	'Designation'                  => 'Puesto',
	'Website'                      => 'Página Web',
	'Industry'                     => 'Actividad',
	'Lead Status'                  => 'Estado del Prospecto',
	'No Of Employees'              => 'Número de Empleados',
	'--None--'                     => '-----',
	'Mr.'                          => 'Sr.',
	'Ms.'                          => 'Sra.',
	'Mrs.'                         => 'Srta.',
	'Dr.'                          => 'Dr.',
	'Prof.'                        => 'Prof.',
	'Attempted to Contact'         => 'Intentado contactar',
	'Cold'                         => 'Frío',
	'Contact in Future'            => 'Contactar más adelante',
	'Contacted'                    => 'Contactado',
	'Hot'                          => 'Caliente',
	'Junk Lead'                    => 'Prospecto basura',
	'Lost Lead'                    => 'Prospecto fallido',
	'Not Contacted'                => 'No Contactado',
	'Pre Qualified'                => 'Pre calificado',
	'Qualified'                    => 'Calificado',
	'Warm'                         => 'Tibio',
	'LBL_CONVERT_LEAD'             => 'Integrar Prospecto:',
	'LBL_TRANSFER_RELATED_RECORD'  => 'Transferir registro relacionado a',
	'LBL_CONVERT_LEAD_ERROR'       => 'Necesita tener habilitado el módulo de Cuentas o Contactos para iniciar con la integración del Prospecto',
	'LBL_LEADS_FIELD_MAPPING_INCOMPLETE' => 'No se han vinculado todos los campos obligatorios',
	'LBL_LEADS_FIELD_MAPPING'      => 'Mapeo de campos de Prospectos',
	'LBL_CUSTOM_FIELD_MAPPING'     => 'Mapeo de campos a la medida',
    'LBL_IMAGE_INFORMATION' => 'Foto de Perfil',
    'Lead Image' => 'Imagen del Prospecto',
	

  'Phone' => 'Teléfono principal',
  'Secondary Email' => 'Correo electrónico secundario',
  'Email' => 'Correo electrónico principal',

);
$jsLanguageStrings = array(
	'JS_SELECT_CONTACTS'           => 'Seleccione un contacto para proceder',
	'JS_SELECT_ORGANIZATION'       => 'Seleccione una cuenta para proceder',
	'JS_SELECT_ORGANIZATION_OR_CONTACT_TO_CONVERT_LEAD' => 'Para iniciar la etapa de integración se requiere la selección de un Contacto o Cuenta',
);